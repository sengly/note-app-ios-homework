//
//  NoteGridCollectionViewCell.swift
//  NoteApp
//
//  Created by Sengly Sun on 12/11/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import UIKit

class NoteGridCollectionViewCell: UICollectionViewCell {

    @IBOutlet var descriptionTextView: UITextView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

}

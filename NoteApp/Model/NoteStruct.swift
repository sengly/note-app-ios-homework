//
//  NoteStruct.swift
//  NoteApp
//
//  Created by Sengly Sun on 12/11/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import Foundation

struct Notes {
    var title: String
    var description: String
}

let allNote = [
    Notes(title: "HI", description: "hello Hello my name is sengly sun do you know my name I am a legendary"),
    Notes(title: "HI", description: "hello Hello my name is sengly sun do you know my name I am a legendary"),
    Notes(title: "HI", description: "hello Hello my name is sengly sun do you know my name I am a legendary"),
    Notes(title: "HI", description: "hello Hello my name is sengly sun do you know my name I am a legendary")
]

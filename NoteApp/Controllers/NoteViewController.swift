//
//  NoteViewController.swift
//  NoteApp
//
//  Created by Sengly Sun on 12/11/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import UIKit

class NoteViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    @IBOutlet var notes: UILabel!
    @IBOutlet var titleLabel: UILabel!
    
    
    @IBOutlet var collectionView: UICollectionView!
    
   
    var collectionViewFlowLayout: UICollectionViewFlowLayout!
    let cellIndentifier: String = "noteGridCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        setUpCollectionView()
        
        notes.text = "notes".localizedString()
        titleLabel.text = "Notes".localizedString()
    }
    
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setUpCollectViewCellItemSize()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allNote.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIndentifier, for: indexPath) as! NoteGridCollectionViewCell
//        cell.dateLabel.text = "today"
        cell.descriptionTextView.text = allNote[indexPath.row].description
        cell.titleLabel.text = allNote[indexPath.row].title
        return cell
    }
    
    private func setUpCollectionView() {
        collectionView.register(UINib(nibName: "NoteGridCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: cellIndentifier)
    }
    
    
    //set up item size in collection view
    private func setUpCollectViewCellItemSize(){
        if collectionViewFlowLayout == nil {
            let itemPerRow: CGFloat =  2
            let lineSpacing: CGFloat = 10
            let itemSpacing: CGFloat = 10
            
            let width = (collectionView.frame.width - (itemPerRow - 1) * (itemSpacing)) / itemPerRow
            let height = width
            print("width: \(width) height: \(height)")
            collectionViewFlowLayout = UICollectionViewFlowLayout()
            collectionViewFlowLayout.itemSize = CGSize(width: width, height: height)
            collectionViewFlowLayout.sectionInset = UIEdgeInsets.zero
            collectionViewFlowLayout.scrollDirection = .vertical
            collectionViewFlowLayout.minimumLineSpacing = lineSpacing
            collectionViewFlowLayout.minimumInteritemSpacing = itemSpacing
            
            
            collectionView.setCollectionViewLayout(collectionViewFlowLayout, animated: true)
        }
    }
    
    //Localization button bar
    @IBAction func switchLanguage(_ sender: Any) {
        let buttonBarSwitchLanguage = UIAlertController()
        
        let khmer = UIAlertAction(title: "Khmer", style: .default) { (_) in
            LanguageApp.shared.chooseLanguage(language: .khmer)
        }
        
        let english = UIAlertAction(title: "English", style: .default) { (_) in
            LanguageApp.shared.chooseLanguage(language: .english)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (_) in
            
        }
        
        buttonBarSwitchLanguage.addAction(khmer)
        buttonBarSwitchLanguage.addAction(english)
        buttonBarSwitchLanguage.addAction(cancel)
        
        present(buttonBarSwitchLanguage, animated: true, completion: nil)
        
    }
    
    //Switch note view mode button
    @IBAction func switchViewMode(_ sender: Any) {
        let buttonSwitchViewMode = UIAlertController()
        let grid = UIAlertAction(title: "View as grid", style: .default) { (_) in
        }
        
        let row = UIAlertAction(title: "View as row", style: .default) { (_) in
            
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (_) in
            
        }
        
        buttonSwitchViewMode.addAction(grid)
        buttonSwitchViewMode.addAction(row)
        buttonSwitchViewMode.addAction(cancel)
        
        present(buttonSwitchViewMode, animated: true, completion: nil)
    }
}

//Extension String to localize
extension String{
    func localizedString() -> String {
        let path = Bundle.main.path(forResource: LanguageApp.shared.language, ofType: "lproj")
        let bundle = Bundle(path: path!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle! , value: self, comment: self)
    }
}

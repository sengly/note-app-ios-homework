//
//  NoteEditingViewController.swift
//  NoteApp
//
//  Created by Sengly Sun on 12/11/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import UIKit

class NoteEditingViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //Editor Option action sheet
    @IBAction func editorOption(_ sender: Any) {
        let editorOption = UIAlertController()
        
        let takePhoto = UIAlertAction(title: "Take Photo", style: .default) { (_) in
        }
        
        let ChooseImage = UIAlertAction(title: "Choose Image", style: .default) { (_) in
            
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (_) in
            
        }
        let drawing = UIAlertAction(title: "Drawing", style: .default) { (_) in
        }
        
        let recording = UIAlertAction(title: "Recording", style: .default) { (_) in
            
        }
        
        let checkBoxs = UIAlertAction(title: "Checkboxs", style: .default) { (_) in
            
        }
        
        editorOption.addAction(takePhoto)
        editorOption.addAction(ChooseImage)
        editorOption.addAction(drawing)
        editorOption.addAction(recording)
        editorOption.addAction(checkBoxs)
        editorOption.addAction(cancel)
        
        present(editorOption, animated: true, completion: nil)
    }
    
    //Note option action sheet
    @IBAction func noteOption(_ sender: Any) {
        let noteOption = UIAlertController()
        
        let delete = UIAlertAction(title: "Delete", style: .default) { (_) in
        }
        
        let makeACopy = UIAlertAction(title: "Make a copy", style: .default) { (_) in
            
        }
        
        let send = UIAlertAction(title: "Send", style: .default) { (_) in
        }
        
        let collarbrators = UIAlertAction(title: "Collarbrators", style: .default) { (_) in
            
        }
        
        let labels = UIAlertAction(title: "Labels", style: .default) { (_) in
            
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (_) in
            
        }
        noteOption.addAction(delete)
        noteOption.addAction(makeACopy)
        noteOption.addAction(send)
        noteOption.addAction(collarbrators)
        noteOption.addAction(labels)
        noteOption.addAction(cancel)
        
        present(noteOption, animated: true, completion: nil)
    }
    

}

//
//  LanguageApp.swift
//  NoteApp
//
//  Created by Sengly Sun on 12/12/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import Foundation

class LanguageApp{
    static var shared = LanguageApp()
    
    var language: String{
        return UserDefaults.standard.string(forKey: "lang") ?? "en"
    }
    
    func chooseLanguage(language: Language){
        UserDefaults.standard.set(language.rawValue,forKey: "lang")
    }
}

enum Language: String{
    case khmer = "km"
    case english = "en"
}
